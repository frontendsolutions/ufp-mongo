*** Settings ***
Library           MongoDBLibrary

*** Variables ***
${MDB_HOST}       ufp-mongo
${MDBUSERNAME}    user
${MDBUSERPASSWORD}    user123
${MDBDATABASE}    UFPDefault
${MDBHostUserAdmin}    mongodb://admin:admin123@${MDB_HOST}
${MDBHostUser}    mongodb://${MDBUSERNAME}:${MDBUSERPASSWORD}@${MDB_HOST}/${MDBDATABASE}
${MDBPort}        27017

*** Test Cases ***
Connect-Disconnect Admin
    [Tags]    regression
    Comment    Connect to MongoDB Server
    Connect To MongoDB    ${MDBHostUserAdmin}    ${MDBPort}
    Comment    Disconnect from MongoDB Server
    Disconnect From MongoDB

Connect-Disconnect User
    [Tags]    regression
    Comment    Connect to MongoDB Server
    Connect To MongoDB    ${MDBHostUser}    ${MDBPort}
    Comment    Disconnect from MongoDB Server
    Disconnect From MongoDB

Get MongoDB Databases
    [Tags]    regression
    Comment
    Connect To MongoDB    ${MDBHostUserAdmin}    ${MDBPort}
    Comment    Retrieve a list of databases on the MongoDB server
    @{output} =    Get MongoDB Databases
    Log Many    @{output}
    Comment    Verify that db name is contained in the list output
    Should Contain    ${output}    admin
    Should Contain    ${output}    local
    Comment    Log as a list
    Log    ${output}
    Comment    Disconnect from MongoDB Server
    Disconnect From MongoDB

Save MongoDB Records
    [Tags]    regression
    ${MDBDB} =    Set Variable    UFPDefault
    ${MDBColl} =    Set Variable    foo
    Comment    Connect to MongoDB Server
    Connect To MongoDB    ${MDBHostUser}    ${MDBPort}
    Comment    Get current record count in collection to ensure that count increases correctly
    ${CurCount} =    Get MongoDB Collection Count    ${MDBDB}    ${MDBColl}
    ${output} =    Save MongoDB Records    ${MDBDB}    ${MDBColl}    {"timestamp":1, "msg":"Hello 1"}
    Log    ${output}
    Set Suite Variable    ${recordno1}    ${output}
    ${output} =    Save MongoDB Records    ${MDBDB}    ${MDBColl}    {"timestamp":2, "msg":"Hello 2"}
    Log    ${output}
    Set Suite Variable    ${recordno2}    ${output}
    ${output} =    Save MongoDB Records    ${MDBDB}    ${MDBColl}    {"timestamp":3, "msg":"Hello 3"}
    Log    ${output}
    Set Suite Variable    ${recordno3}    ${output}
    Comment    Verify that the record count increased by the number of records saved above (3)
    ${output} =    Get MongoDB Collection Count    ${MDBDB}    ${MDBColl}
    Should Be Equal    ${output}    ${${CurCount} + 3}
    Disconnect From MongoDB
