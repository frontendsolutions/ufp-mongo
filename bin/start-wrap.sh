#!/usr/bin/env bash

COLOR1="\033[1;34m"
COLOR2="\033[0;33m"
NOCOLOR="\033[0m" # No Color
log(){
	echo -e "[${COLOR1}$(date '+%Y-%m-%d %H:%M:%S')${NOCOLOR}] [${COLOR2}${1}${NOCOLOR}]"
}
  env

cat /wraps/banner/banner.txt

log "Init Mongo Wrapper"

envsubst </wraps/mongo-init-template.js>>/docker-entrypoint-initdb.d/init-mongo.js


log "Generated config is"
cat /docker-entrypoint-initdb.d/init-mongo.js

log "Starting mongo"

docker-entrypoint.sh "${@}"
