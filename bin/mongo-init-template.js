db.auth('${MONGO_INITDB_ROOT_USERNAME}', '${MONGO_INITDB_ROOT_PASSWORD}')

// db = db.getSiblingDB('${MONGO_INITDB_DATABASE}')

db.createUser({
    user: '${MONGO_INITDB_DATABASE_USERNAME}',
    pwd: '${MONGO_INITDB_DATABASE_PASSWORD}',
    roles: [
        {
            role: 'dbAdmin',
            db: '${MONGO_INITDB_DATABASE}'
        } ,
        {
            role: 'readWrite',
            db: '${MONGO_INITDB_DATABASE}'
        }
    ]
});
