FROM mongo:3.4
RUN apt-get update\
          && apt-get  install -y  gettext-base \
            && apt-get clean \
            && rm -rf /var/lib/apt/lists/*
ENV MONGO_INITDB_ROOT_USERNAME admin-user
ENV MONGO_INITDB_ROOT_PASSWORD admin-password
ENV MONGO_INITDB_DATABASE admin
ENV MONGO_INITDB_DATABASE_USER admin
ENV MONGO_INITDB_DATABASE_PASSWORD admin123

ADD ./bin /wraps



ENTRYPOINT ["/wraps/start-wrap.sh"]
EXPOSE 27017
CMD ["mongod"]
